package com.mospolytech.generator

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    var counter = 1
    val texts: Array<String> =
        arrayOf(
            "Настала неделя счастливого кодинга! Время компляции уменьшено вдвое",
            "Жизнь стала налаживаться - AndroidStudio жрет в 3 раза меньше памяти",
            "На этой неделе тебе придется кодить на Ассэмблере. Это нужно просто пережить",
            "Все тесты будут выполняться, что бы ты не написал. Развлекайся!",
            "Путин подпишет законопроект, согласно которому все программисты обязаны писать в блокноте",
            "Произойдет межгалактический сбой, и тестовая версия твоего приложения попадет в продакшн. Закрывай предсказания СРОЧНО!",
            "\"Почему ты тут сидишь? Работы мало? Дать еще пару задач?\" - руководство сегодня не в духе",
            "\"Мне выпало крутое предсказание! У всех сегодня выходной!\" - не благодари, бро",
            "На этой неделе ты изучишь Арч Линукс! Мои соболезнования!",
            "Китайские устройства объявят вне закона и больше не придется оптимизировать под них приложения!"
        )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rand = Random.nextInt(0, 10)
        text.text = texts.get(rand)
        number.text = (rand + 1).toString();

        getNew.setOnClickListener {
            val rand = Random.nextInt(0, 10);
            number.text = (rand + 1).toString();
            text.text = texts.get(rand)
            counter++;
            if (counter == 16) {
                number.text = "256"
                text.text =
                    "КОСТЫЛЬ МНЕ В КОД, ТЕБЕ ВЫПАЛО СЧАСТЛИВОЕ ЧИСЛО - 256! ТЫ СТАНЕШЬ БОГОМ ПРОГРАММИРОВАНИЯ И ПОКОРИШЬ МИР"
            }
        }
    }
}
